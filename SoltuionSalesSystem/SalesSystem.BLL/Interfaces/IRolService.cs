﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SalesSystem.Entity;

namespace SalesSystem.BLL.Interfaces
{
	public interface IRolService
	{
		Task<List<Rol>> Lista();
	}
}
