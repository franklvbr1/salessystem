﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SalesSystem.BLL.Interfaces;
using SalesSystem.DAL.Interfaces;
using SalesSystem.Entity;

namespace SalesSystem.BLL.Implementation
{
	public class NegocioService : INegocioService
	{
		private readonly IGenericRepository<Negocio> _repository;
		private readonly IFireBaseService _fireBaseService;

		public NegocioService(IGenericRepository<Negocio> repository, IFireBaseService fireBaseService)
		{
			_repository = repository;
			_fireBaseService = fireBaseService;
		}

		public async Task<Negocio> Obtener()
		{
			try
			{
				Negocio negocio_encontrado = await _repository.Obtener(n=> n.IdNegocio == 1);
				return negocio_encontrado;
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
		public async Task<Negocio> GuardarCambios(Negocio entidad, Stream Logo = null, string NombreLogo = "")
		{
			try
			{
				Negocio negocio_encontrado = await _repository.Obtener(n => n.IdNegocio == 1);

				negocio_encontrado.NumeroDocumento = entidad.NumeroDocumento;
				negocio_encontrado.Nombre = entidad.Nombre;
				negocio_encontrado.Correo = entidad.Correo;
				negocio_encontrado.Direccion = entidad.Direccion;
				negocio_encontrado.Telefono = entidad.Telefono;
				negocio_encontrado.PorcentajeImpuesto = entidad.PorcentajeImpuesto;
				negocio_encontrado.SimboloMoneda = entidad.SimboloMoneda;

				negocio_encontrado.NombreLogo = negocio_encontrado.NombreLogo == "" ? NombreLogo : negocio_encontrado.NombreLogo;

				if (Logo != null)
				{
					string urlFoto = await _fireBaseService.SubirStorage(Logo, "carpeta_logo", negocio_encontrado.NombreLogo);
					negocio_encontrado.UrlLogo = urlFoto;
				}

				await _repository.Editar(negocio_encontrado);
				return negocio_encontrado;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		
	}
}
