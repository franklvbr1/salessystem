﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using SalesSystem.BLL.Interfaces;
using SalesSystem.DAL.Interfaces;
using SalesSystem.Entity;

namespace SalesSystem.BLL.Implementation
{
	public class VentaService : IVentaService
	{
		private readonly IGenericRepository<Producto> _repositoryProducto;
		private readonly IVentaRepository _repositoryVenta;

		public VentaService(IGenericRepository<Producto> repositoryProducto, IVentaRepository repositoryVenta)
		{
			_repositoryProducto = repositoryProducto;
			_repositoryVenta = repositoryVenta;
		}


		public async Task<List<Producto>> ObtenerProductos(string busqueda)
		{
			IQueryable<Producto> query = await _repositoryProducto.Consultar(
				p => p.EsActivo == true &&
				p.Stock > 0 &&
				string.Concat(p.CodigoBarra, p.Marca, p.Descripcion).Contains(busqueda));

			return query.Include(c => c.IdCategoriaNavigation).ToList();
		}
		public async Task<Venta> Registrar(Venta entidad)
		{
			try
			{
				return await _repositoryVenta.Registrar(entidad);
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
		public async Task<List<Venta>> Historial(string numeroVenta, string fechaInicio, string fechaFin)
		{
			IQueryable<Venta> query = await _repositoryVenta.Consultar();
			fechaInicio = fechaInicio is null ? "" : fechaInicio;
			fechaFin = fechaFin is null ? "" : fechaFin;

			if(fechaInicio != "" && fechaFin != "")
			{
				DateTime startF = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", new CultureInfo("es-Pe"));
				DateTime endF = DateTime.ParseExact(fechaFin, "dd/MM/yyyy", new CultureInfo("es-Pe"));

				return query.Where(v =>
									v.FechaRegistro.Value.Date >= startF.Date &&
									v.FechaRegistro.Value.Date <= endF.Date
					).Include(tdv => tdv.IdTipoDocumentoVentaNavigation)
					.Include(u => u.IdUsuarioNavigation)
					.Include(dv => dv.DetalleVenta)
					.ToList();
			}
			else
			{
				return query.Where(v =>v.NumeroVenta ==  numeroVenta
					).Include(tdv => tdv.IdTipoDocumentoVentaNavigation)
					.Include(u => u.IdUsuarioNavigation)
					.Include(dv => dv.DetalleVenta)
					.ToList();
			}

		}
		public async Task<Venta> Detalle(string numeroVenta)
		{
			IQueryable<Venta> query = await _repositoryVenta.Consultar(v =>v.NumeroVenta == numeroVenta);

		return query
				.Include(tdv => tdv.IdTipoDocumentoVentaNavigation)
				.Include(u => u.IdUsuarioNavigation)
				.Include(dv => dv.DetalleVenta)
				.First();

		}
		public async Task<List<DetalleVenta>> Reporte(string fechaInicio, string fechaFin)
		{
			DateTime startF = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", new CultureInfo("es-Pe"));
			DateTime endF = DateTime.ParseExact(fechaFin, "dd/MM/yyyy", new CultureInfo("es-Pe"));

			List<DetalleVenta> lista = await _repositoryVenta.Reporte(startF, endF);

			return lista;

		}
	}
}
