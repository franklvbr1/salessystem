﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SalesSystem.BLL.Interfaces;
using SalesSystem.DAL.Interfaces;
using SalesSystem.Entity;

namespace SalesSystem.BLL.Implementation
{
	public class MenuService : IMenuService
	{
		private readonly IGenericRepository<Menu> _menuRepository;
		private readonly IGenericRepository<RolMenu> _rolmenuRepository;
		private readonly IGenericRepository<Usuario> _usuarioRepository;

		public MenuService(IGenericRepository<Menu> menuRepository, IGenericRepository<RolMenu> rolmenuRepository, IGenericRepository<Usuario> usuarioRepository)
		{
			_menuRepository = menuRepository;
			_rolmenuRepository = rolmenuRepository;
			_usuarioRepository = usuarioRepository;
		}

		public async Task<List<Menu>> ObtenerMenus(int idUsuario)
		{
			IQueryable<Usuario> tbUsuario = await _usuarioRepository.Consultar(u => u.IdUsuario == idUsuario);
			IQueryable<RolMenu> tbRolMenu = await _rolmenuRepository.Consultar();
			IQueryable<Menu> tbMenu = await _menuRepository.Consultar();

			IQueryable<Menu> MenuPadre = (from u in tbUsuario
										  join rm in tbRolMenu on u.IdRol equals rm.IdRol
										  join m in tbMenu on rm.IdMenu equals m.IdMenu
										  join mpadre in tbMenu on m.IdMenuPadre equals mpadre.IdMenu
										  select mpadre).Distinct().AsQueryable();

			IQueryable<Menu> MenuHijos = ( from u in tbUsuario
															  join rm in tbRolMenu on u.IdRol equals rm.IdRol
															  join m in tbMenu on rm.IdMenu equals  m.IdMenu
															  where m.IdMenu != m.IdMenuPadre
															  select m).Distinct().AsQueryable();

			List<Menu> listaMenu = (from mpadre in MenuPadre
												select new Menu()
												{
													Descripcion = mpadre.Descripcion,
													Icono = mpadre.Icono,
													Controlador = mpadre.Controlador,
													PaginaAccion = mpadre.PaginaAccion,
													InverseIdMenuPadreNavigation = (from mhijo in MenuHijos
																					where mhijo.IdMenuPadre == mpadre.IdMenu
																					select mhijo).ToList()
												}).ToList();

			return listaMenu;
		}
	}
}
