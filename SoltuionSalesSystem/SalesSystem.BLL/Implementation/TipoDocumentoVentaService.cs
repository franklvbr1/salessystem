﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SalesSystem.BLL.Interfaces;
using SalesSystem.DAL.Interfaces;
using SalesSystem.Entity;

namespace SalesSystem.BLL.Implementation
{
	public class TipoDocumentoVentaService : ITipoDocumentoVentaService
	{
		private readonly IGenericRepository<TipoDocumentoVenta> _repository;

		public TipoDocumentoVentaService(IGenericRepository<TipoDocumentoVenta> tipoDocumentoVentaRepository)
		{
			_repository = tipoDocumentoVentaRepository;
		}

		public async Task<List<TipoDocumentoVenta>> Lista()
		{
			IQueryable<TipoDocumentoVenta> query = await _repository.Consultar();
			return query.ToList();
		}
	}
}
