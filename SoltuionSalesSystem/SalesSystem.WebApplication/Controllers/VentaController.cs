﻿using Microsoft.AspNetCore.Mvc;

using AutoMapper;
using SalesSystem.WebApplication.Models.ViewModels;
using SalesSystem.WebApplication.Utilidades.Response;
using SalesSystem.BLL.Interfaces;
using SalesSystem.Entity;

using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace SalesSystem.WebApplication.Controllers
{
	[Authorize]
	public class VentaController : Controller
    {
        private readonly ITipoDocumentoVentaService _tipoDocumentoVentaService;
        private readonly IVentaService _ventaService;
        private readonly IMapper _mapper;
		private readonly IConverter _converter;

        public VentaController(ITipoDocumentoVentaService tipoDocumentoVentaService, IVentaService ventaService, IMapper mapper, IConverter converter)
        {
            _tipoDocumentoVentaService = tipoDocumentoVentaService;
            _ventaService = ventaService;
            _mapper = mapper;
			_converter = converter;

		}

        public IActionResult NuevaVenta()
        {
            return View();
        }

		public IActionResult HistorialVenta()
		{
			return View();
		}

        [HttpGet]
		public async Task<IActionResult> ListaTipoDocumentoVenta()
		{
            List<VMTipoDocumentoVenta> vmListaTipoDocumento = _mapper.Map<List<VMTipoDocumentoVenta>>(await _tipoDocumentoVentaService.Lista());
            return StatusCode(StatusCodes.Status200OK, vmListaTipoDocumento);
		}

		[HttpGet]
		public async Task<IActionResult> ObtenerProductos(string busqueda)
		{
            List<VMProducto> vmListaProductos = _mapper.Map<List<VMProducto>>(await _ventaService.ObtenerProductos(busqueda));
			return StatusCode(StatusCodes.Status200OK, vmListaProductos);
		}

		[HttpPost]
		public async Task<IActionResult> RegistrarVenta([FromBody] VMVenta modelo)
		{
			GenericResponse<VMVenta> gResposne = new GenericResponse<VMVenta>();

			try
			{

				ClaimsPrincipal claimUser = HttpContext.User;

				string idUsuario = claimUser.Claims
											.Where(c => c.Type == ClaimTypes.NameIdentifier)
											.Select(c => c.Value).SingleOrDefault();
				modelo.IdUsuario = int.Parse(idUsuario);

				Venta venta_creada = await _ventaService.Registrar(_mapper.Map<Venta>(modelo));
				modelo = _mapper.Map<VMVenta>(venta_creada);

				gResposne.Estado = true;
				gResposne.Objeto = modelo;

			}
			catch (Exception ex)
			{
				gResposne.Estado = false;
				gResposne.Mensaje = ex.Message;
			}

			return StatusCode(StatusCodes.Status200OK, gResposne);
		}

		[HttpGet]
		public async Task<IActionResult> Historial(string numeroVenta, string fechaInicio, string fechaFin)
		{
			List<VMVenta> vmHistorialVenta = _mapper.Map<List<VMVenta>>(await _ventaService.Historial(numeroVenta,fechaInicio,fechaFin));
			return StatusCode(StatusCodes.Status200OK, vmHistorialVenta);
		}

		public IActionResult MostrarPDFVenta (string numeroVenta)
		{
			string urlPlantillaVista = $"{this.Request.Scheme}://{this.Request.Host}/Plantilla/PDFVenta?numeroVenta={numeroVenta}";

			var pdf = new HtmlToPdfDocument()
			{
				GlobalSettings = new GlobalSettings()
				{
					PaperSize = PaperKind.A4,
					Orientation = Orientation.Portrait,
				},
				Objects =
				{
					new ObjectSettings()
					{
						Page = urlPlantillaVista
					}
				}
			};

			var archivoPDF = _converter.Convert(pdf);
			return File(archivoPDF, "application/pdf");

		}

	}
}
