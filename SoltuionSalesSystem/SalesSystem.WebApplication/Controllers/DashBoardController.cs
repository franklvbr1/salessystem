﻿using Microsoft.AspNetCore.Mvc;

using SalesSystem.WebApplication.Models.ViewModels;
using SalesSystem.WebApplication.Utilidades.Response;
using SalesSystem.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace SalesSystem.WebApplication.Controllers
{
	[Authorize]
	public class DashBoardController : Controller
	{
		private readonly IDashoBoardService _dashoBoardService;

		public DashBoardController(IDashoBoardService dashoBoardService)
		{
			_dashoBoardService = dashoBoardService;
		}

		public IActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> ObtenerResumen()
		{
			GenericResponse<VMDashBoard> gResponse = new GenericResponse<VMDashBoard>();
			try
			{
				VMDashBoard vmDashboard = new VMDashBoard();
				vmDashboard.TotalVentas = await _dashoBoardService.TotalVentasUltimaSemana();
				vmDashboard.TotalIngresos = await _dashoBoardService.TotalIngresosUltimaSemana();
				vmDashboard.TotalProductos= await _dashoBoardService.TotalProductos();
				vmDashboard.TotalCategorias = await _dashoBoardService.TotalCategorias();

				List<VMVentasSemana> listaVentasSemana = new List<VMVentasSemana>();
				List<VMProductosSemana> listaProdcutosSemana = new List<VMProductosSemana>();

				foreach (KeyValuePair<string,int> item in await _dashoBoardService.VentasUltimaSemana())
				{
					listaVentasSemana.Add(new VMVentasSemana()
					{
						Fecha = item.Key,
						Total = item.Value
					}) ;
				}

				foreach (KeyValuePair<string, int> item in await _dashoBoardService.ProductosTopUltimaSemana())
				{
					listaProdcutosSemana.Add(new VMProductosSemana()
					{
						Producto = item.Key,
						Cantidad = item.Value
					});
				}

				vmDashboard.VentasUltimaSemana = listaVentasSemana;
				vmDashboard.ProductosTopUltimaSemana = listaProdcutosSemana;

				gResponse.Estado = true;
				gResponse.Objeto = vmDashboard;

			}
			catch (Exception ex)
			{
				gResponse.Estado = false;
				gResponse.Mensaje = ex.Message;
			}

			return StatusCode(StatusCodes.Status200OK, gResponse);
		}
	}
}
