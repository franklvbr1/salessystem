﻿using Microsoft.AspNetCore.Mvc;

using SalesSystem.WebApplication.Models.ViewModels;
using SalesSystem.BLL.Interfaces;
using SalesSystem.Entity;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace SalesSystem.WebApplication.Controllers
{
	public class AccesoController : Controller
	{
		private readonly IUsuarioService _usuarioService;

		public AccesoController(IUsuarioService usuarioService)
		{
			_usuarioService = usuarioService;
		}

		public IActionResult Login()
		{
			ClaimsPrincipal claimUser = HttpContext.User;

			if (claimUser.Identity.IsAuthenticated)
			{
				return RedirectToAction("Index", "Home");
			}

			return View();
		}

		public IActionResult RestablecerClave()
		{			

			return View();
		}

		[HttpPost]
		public async  Task<IActionResult> Login(VMUsuarioLogin modelo)
		{

			Usuario usuario_encontrado = await _usuarioService.ObtenerPorCredenciales(modelo.Correo, modelo.Clave);

			if (usuario_encontrado == null)
			{
				ViewData["Mensaje"] = "Usuario y/o contraseña invalidos";
				return View();
			}

			ViewData["Mensaje"] = null;

			List<Claim> claims = new List<Claim>()
			{
				new Claim(ClaimTypes.Name, usuario_encontrado.Nombre),
				new Claim(ClaimTypes.NameIdentifier, usuario_encontrado.IdUsuario.ToString()),
				new Claim(ClaimTypes.Role, usuario_encontrado.IdRol.ToString()),
				new Claim("UrlFoto", usuario_encontrado.UrlFoto),
			};

			ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

			AuthenticationProperties properties = new AuthenticationProperties()
			{
				AllowRefresh = true,
				IsPersistent = modelo.MantenerSesion
			};

			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
															new ClaimsPrincipal(claimsIdentity),
															properties
															);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public async Task<IActionResult> RestablecerClave(VMUsuarioLogin modelo)
		{

			try
			{
				string urlPlantillaCorreo = $"{this.Request.Scheme}://{this.Request.Host}/Plantilla/RestablecerClave?clave=[clave]";
				bool resultado = await _usuarioService.ReestablecerClave(modelo.Correo, urlPlantillaCorreo);

				if (resultado)
				{
					ViewData["Mensaje"] = "Contraseña restablecida. Revise su correo";
					ViewData["MensajeError"] = null;
				}
				else
				{
					ViewData["MensajeError"] = "Tenemos problemas. Por favor intentelo de nuevo mas tarde";
					ViewData["Mensaje"] = null;
				}

			}
			catch (Exception ex)
			{
				ViewData["MensajeError"] = ex.Message;
				ViewData["Mensaje"] = null;
			}
			return View();
		}
		
	}
}
